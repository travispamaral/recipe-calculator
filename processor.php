<?php
require_once 'CONFIG.php';
$SQLi = mysqli_connect( HOST , USERNAME , PASSWORD , DATABASE );
?>
<?php
$sofarsogood = TRUE;
switch($_POST['action']):

	case 'recipe_new':
			$query =	"INSERT INTO
							recipes
						SET
							`id` = '',
							`parent_id` = 0,
							`name` = '".$_POST['recipe_name']."',
							`percent` = 0
						;";
			$result = $SQLi->query($query);
			if($result):
				$created_row_id = $SQLi->insert_id;
				foreach( (array)$_POST['new_ingredient'] as $key => $ingredient ):
					$query =	"INSERT INTO
									recipes
								SET
									`id` = '',
									`parent_id` = ".$created_row_id.",
									`name` = '".$ingredient."',
									`percent` = ".$_POST['percent'][$key]."
								;";
					$result = $SQLi->query($query);
				endforeach;
			else:
				$sofarsogood = FALSE;
			endif;
			break;

	case 'recipe_modify':
			if( strtolower($_POST['recipe_name']) != 'delete' ):
				$query =	"UPDATE
								recipes
							SET
								`name` = '".$_POST['recipe_name']."'
							WHERE
								`id` = ".$_POST['recipe_id']."
							LIMIT
								1
							;";
			else:
				$query =	"DELETE FROM
								recipes
							WHERE
								`id` = ".$_POST['recipe_id']."
							LIMIT
								1
							;";
				$result = $SQLi->query($query);
				if(!$result) $sofarsogood = FALSE;
				$query =	"DELETE FROM
								recipes
							WHERE
								`parent_id` = ".$_POST['recipe_id']."
							;";
				$result = $SQLi->query($query);
				if(!$result) $sofarsogood = FALSE;
			endif;
			$result = $SQLi->query($query);
			if(!$result) $sofarsogood = FALSE;
			foreach( (array)$_POST['ingredient'] as $key => $ingredient ):
				if( strtolower($ingredient) != 'delete' ):
					$query =	"UPDATE
									recipes
								SET
									`name` = '".$ingredient."',
									`percent` = ".$_POST['percent'][$key]."
								WHERE
									`id` = ".$_POST['id'][$key]."
								LIMIT
									1
								;";
				else:
					$query =	"DELETE FROM
									recipes
								WHERE
									`id` = ".$_POST['id'][$key]."
								LIMIT
									1
								;";
				endif;
				$result = $SQLi->query($query);
				if(!$result) $sofarsogood = FALSE;
			endforeach;
			foreach( (array)$_POST['new_ingredient'] as $key => $new_ingredient ):
					$query =	"INSERT INTO
									recipes
								SET
									`id` = '',
									`parent_id` = ".$_POST['recipe_id'].",
									`name` = '".$new_ingredient."',
									`percent` = ".$_POST['percent'][$key]."
								;";
				$result = $SQLi->query($query);
				if(!$result) $sofarsogood = FALSE;
			endforeach;
			break;

	case 'add_new_user':
			$query =	"INSERT INTO
							users
						SET
							`id` = '',
							`is_admin` = '".(bool)$_POST['is_admin']."',
							`user` = '".$_POST['user']."',
							`pass` = '".$_POST['pass']."'
						;";
			$result = $SQLi->query($query);
			if(!$result) $sofarsogood = FALSE;
			break;

	case 'modify_user':
			if( strtolower($_POST['user']) != 'delete' ):
				$query =	"UPDATE
								users
							SET
								`is_admin` = ".($_POST['is_admin']?1:0).",
								`user` = '".$_POST['user']."',
								`pass` = '".$_POST['pass']."'
							WHERE
								`id` = ".$_POST['id']."
							LIMIT
								1
							;";
			else:
				$query =	"DELETE FROM
								users
							WHERE
								`id` = ".$_POST['id']."
							LIMIT
								1
							;";
			endif;
			$result = $SQLi->query($query);
			if(!$result) $sofarsogood = FALSE;
			break;

	case 'doit':
			if(empty($_POST['user'])||empty($_POST['pass'])):
				$sofarsogood = FALSE;
				break;
			endif;
			$query =	"SELECT * FROM
							users
						WHERE
							LOWER(`user`) = '".strtolower($_POST['user'])."' AND
							LOWER(`pass`) = '".strtolower($_POST['pass'])."'
						LIMIT
							1
						;";
			$result = $SQLi->query($query);
			if(!$result || $result->num_rows == 0):
				$sofarsogood = FALSE;
			else:
				$user = $result->fetch_assoc();
			endif;
			break;

	default:
			$sofarsogood = FALSE;
			break;
	
endswitch;

$SQLi->close();
$response = array();
if($_POST['action']=='doit' && $sofarsogood):
	$response['cook'] = strtolower($_POST['user']);
	if( $user['is_admin']):
		$response['boss'] = 1;
	endif;
	if( !empty($response['cook'])):
		$response['success'] = 1;
	endif;
else:
	$response['success'] = ($sofarsogood?1:0);
endif;
echo json_encode($response);
?>