-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: trabucco
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `recipes`
--

DROP TABLE IF EXISTS `recipes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recipes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `name` char(40) NOT NULL,
  `percent` decimal(4,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipes`
--

LOCK TABLES `recipes` WRITE;
/*!40000 ALTER TABLE `recipes` DISABLE KEYS */;
INSERT INTO `recipes` VALUES (47,42,'number',3.80),(46,42,'number',4.70),(49,48,'Apple Cider Vinegar',6.80),(48,0,'Silverado',0.00),(56,48,'Red Oak',3.40),(55,48,'Raisin',4.60),(54,48,'Peanut Butter',2.30),(53,48,'Honey',3.40),(52,48,'Graham Cracker',4.60),(34,0,'Blackstar',0.00),(35,34,'Graham Cracker Dark (TPA)',9.30),(36,34,'Apple Cider Vinegar',4.70),(37,34,'Banana Cream (TPA)',2.30),(38,34,'Cowboy Blend (TPA)',4.70),(39,34,'Marshmallow (TPA)',4.70),(40,34,'Peanut Butter (TPA)',4.70),(41,34,'Tobacco Extract',14.00),(50,48,'Cowboy Blend',6.80),(57,48,'Tobacco Extract',13.60),(58,0,'Portola',0.00),(59,58,'Apple Cider Vinegar',4.50),(60,58,'Cowboy Blend',4.50),(61,58,'Honey',4.50),(62,58,'Malted Milk',4.50),(63,58,'Vanilla Custard',10.10),(72,58,'Graham Craker',4.50),(65,58,'Tobacco Extract',13.48),(66,0,'Capistrano',0.00),(67,66,'Apple Cider Vinegar',9.10),(68,66,'Blueberry',11.40),(69,66,'French Vanilla',6.80),(70,66,'Honey',4.60),(71,66,'Tobacco Extract',13.60),(74,0,'Balboa',0.00),(75,74,'Tobacco Extract',19.00),(76,74,'French Vanilla',4.00),(77,74,'Smooth',0.50),(78,74,'Rasberry',0.50),(79,74,'Mild Black',2.50),(80,74,'Wild Blueberry',5.50),(81,74,'Apple Cider Vinegar',3.10),(82,0,'Portola V2',0.00),(83,82,'Tobacco Extract',25.00),(84,82,'Vanilla Custard V2',6.00),(85,82,'Graham Cracker V2',0.50),(86,82,'Cowboy Blend',4.00),(87,82,'Apple Cider Vinegar',4.00),(88,0,'RESERVE - Blackstar',0.00),(89,88,'Tobacco Extract',24.00),(90,88,'Graham Cracker',9.30),(91,88,'Peanut Butter',4.70),(92,88,'Marshmallow',4.70),(93,88,'Banana Cream',2.30),(94,88,'Western Blend',4.70),(95,88,'Apple Cider Vinegar',4.70),(96,0,'RESERVE - Portola',0.00),(97,96,'Tobacco Extract',35.00),(98,96,'Graham Cracker V2',0.50),(99,96,'Vanilla Custard V2',6.00),(100,96,'Apple Cider Vinegar',4.00),(101,96,'Western',4.00);
/*!40000 ALTER TABLE `recipes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `is_admin` tinyint(1) NOT NULL,
  `user` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_2` (`user`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,0,'trabuco','trabucoadmin714'),(2,1,'travis','chucktaylor');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-15 22:46:01
