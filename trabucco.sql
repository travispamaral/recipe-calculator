-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 14, 2015 at 12:13 AM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `trabucco`
--

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

DROP TABLE IF EXISTS `recipes`;
CREATE TABLE IF NOT EXISTS `recipes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `name` char(40) NOT NULL,
  `percent` decimal(4,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `recipes`
--

INSERT INTO `recipes` (`id`, `parent_id`, `name`, `percent`) VALUES
(47, 42, 'number', 3.80),
(46, 42, 'number', 4.70),
(49, 48, 'Apple Cider Vinegar', 6.80),
(48, 0, 'Silverado', 0.00),
(56, 48, 'Red Oak', 3.40),
(55, 48, 'Raisin', 4.60),
(54, 48, 'Peanut Butter', 2.30),
(53, 48, 'Honey', 3.40),
(52, 48, 'Graham Cracker', 4.60),
(34, 0, 'Blackstar', 0.00),
(35, 34, 'Graham Cracker Dark (TPA)', 9.30),
(36, 34, 'Apple Cider Vinegar', 4.70),
(37, 34, 'Banana Cream (TPA)', 2.30),
(38, 34, 'Cowboy Blend (TPA)', 4.70),
(39, 34, 'Marshmallow (TPA)', 4.70),
(40, 34, 'Peanut Butter (TPA)', 4.70),
(41, 34, 'Tobacco Extract', 14.00),
(50, 48, 'Cowboy Blend', 6.80),
(57, 48, 'Tobacco Extract', 13.60),
(58, 0, 'Portola', 0.00),
(59, 58, 'Apple Cider Vinegar', 4.50),
(60, 58, 'Cowboy Blend', 4.50),
(61, 58, 'Honey', 4.50),
(62, 58, 'Malted Milk', 4.50),
(63, 58, 'Vanilla Custard', 10.10),
(72, 58, 'Graham Craker', 4.50),
(65, 58, 'Tobacco Extract', 13.48),
(66, 0, 'Capistrano', 0.00),
(67, 66, 'Apple Cider Vinegar', 9.10),
(68, 66, 'Blueberry', 11.40),
(69, 66, 'French Vanilla', 6.80),
(70, 66, 'Honey', 4.60),
(71, 66, 'Tobacco Extract', 13.60);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `is_admin` tinyint(1) NOT NULL,
  `user` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_2` (`user`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `is_admin`, `user`, `pass`) VALUES
(1, 0, 'trabuco', 'vapebro'),
(2, 1, 'travis', 'chucktaylor');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
