<?php
require_once 'CONFIG.php';
$SQLi = mysqli_connect( HOST , USERNAME , PASSWORD , DATABASE );
?>
<?php if(!$_COOKIE['user']): ?>
<!DOCTYPE HTML PUBLIC>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel=stylesheet href="style.css">

	<script type="text/javascript">
		function doit(me,boss,kill){
			var ex = new Date()
				ex.setTime( (kill?0:ex.getTime()+5*24*60*60*1000) )
			document.cookie = 'user='+escape(me) + '; expires='+ ex.toUTCString()+'; path=/'
			if(boss) document.cookie = 'is_admin='+escape(1) + '; expires='+ ex.toUTCString()+'; path=/'
			window.location.reload()
		}
		function isElement(element){
			var tagname = false
			try{
				tagname = element.tagName.toLowerCase()	//If .tagName is not a property... .toLowerCase() will fail on it
				return true
			}catch(e){
				return false
			}
			return false
		}
		function isInputAndHasNameAndValue(element){
			if(	element.tagName.toLowerCase() == 'input' &&
				element.name != '' &&
				element.value != ''
				)return true;
			return false;
		}
		function submitThis(form){
			var is_element = false
			var url='processor.php'
			var variables = ''

			for (var element in form.elements){
				if(isElement(form.elements[element])){
					if( isInputAndHasNameAndValue(form.elements[element]) ){
						variables+= form.elements[element].name+'='+form.elements[element].value+'&'
					}
				}
			}
			var last_character  = variables.substr(variables.length-1)
			if( last_character == '&') variables = variables.substr(0,variables.length-1)

			var request=new XMLHttpRequest();
				request.open('POST', url+'?nocache='+ new Date().getTime() ,true);  // Cache Control friendly
				request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				request.send(variables)
				request.onreadystatechange = function (aEvt) {
			if (request.readyState == 4) {

				if(request.status == 200)
					response_callback_function(request.responseText)
				 else
					dump("There was some kind of Error with the xml_http_request.");
				}/*end if-else successful request status */

			}/*endif request readystate changed */
			return false
		}

		function response_callback_function(response_text){
			//alert(response_text)
			var JSON_response = JSON.parse(response_text)
			if( JSON_response.success ){
				alert('All Good')
				doit(JSON_response.cook,JSON_response.boss,false)
			}else{
				alert('Try again')
			}
		}
	</script>
</head>
<body class="login">
	<div class="logo"></div>
	<form action="" method="post" onsubmit="return submitThis(this);"><fieldset>
		<input type="text" name="user" placeholder="username" required/><br/>
		<input type="text" name="pass" placeholder="password" required/><br/>
		<input type="hidden" name="action" value="doit"/>
		<button type="submit">Login</button>
	</fieldset></form>
</body>
</html>
<?php exit ?>
<?php endif ?>
<!DOCTYPE HTML PUBLIC>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel=stylesheet href="style.css">
<script type="text/javascript">
	function calulate(){

		var	desired_amount = document.getElementById('desired_amount').value,
			desired_strength = document.getElementById('desired_strength').value,
			desired_nicotine_strength = document.getElementById('desired_nicotine_strength').value;

	}
	function doit(me,boss,kill){
		var ex = new Date()
			ex.setTime( (kill?0:ex.getTime()+5*24*60*60*1000) )
		document.cookie = 'user='+escape(me) + '; expires='+ ex.toUTCString()+'; path=/'
		if(boss) document.cookie = 'is_admin='+escape(1) + '; expires='+ ex.toUTCString()+'; path=/'
		window.location.reload()
	}
</script>
</head>

<body>
<div class="logo"></div>
<script type="text/javascript">
	<?php $recipe = array() ?>
	<?php
	if( $_GET['recipe_id'] ):
		$recipe = array();
		$query =	"SELECT * FROM
						recipes
					WHERE
						`parent_id` = ".$_GET['recipe_id']."
					ORDER BY
						`name` ASC
					;";
		$result = $SQLi->query($query);
		while($ingredient = $result->fetch_assoc()):
			$recipe['ingredients'][$ingredient['name']] = $ingredient['percent'];
		endwhile;
	endif;
	?>
	var	JSON_recipe = JSON.parse('<?php echo str_ireplace("'","\'",json_encode($recipe)) ?>');
</script>

<fieldset><h1>If you want this:</h1>
	<ul class="fields">
		<li>
			<h2 style="text-align:center; margin:0;">
				<form action="" method="GET">
					<select name="recipe_id" onchange="document.forms[0].submit()">
						<option disabled selected value=''>Select a recipe</option>
						<option disabled value=''></option>
					<?php
						$query =	"SELECT * FROM
										recipes
									WHERE
										`parent_id` = 0
									ORDER BY
										`name` ASC
									;";
						$result = $SQLi->query($query);
						while($_recipe = $result->fetch_assoc()):
					?>
						<option <?php echo ($_recipe['id']==$_GET['recipe_id']?'SELECTED':'') ?> value="<?php echo $_recipe['id'] ?>"><?php echo $_recipe['name'] ?></option>
					<?php endwhile ?>
					</select>
				</form>
			</h2>
		</li>
		<li>
			<div class="label">Amount to make:</div>
			<div class="input">
				<input id="desired_amount" type="number" step="10" min="0" value="30" onchange="updateAmounts();"/>
			</div>
			<div class="input_cap">ml</div>
		</li>
		<!--<li style="padding:0;">
			<input style="display:block; width:100%;" onchange="document.getElementById('desired_amount').value = this.value; updateAmounts();" type="range" min="30" max="3000" step="10" value="30" />
		</li>-->
		<li>
			<div class="label">Desired strength:</div>
			<div class="input" style="padding-right:2em;">
				<input type="radio" name="desired_strength" id="desired_strength_3" value="3" onchange="updateAmounts()" />
				<label for="desired_strength_3">3</label>
				<input type="radio" name="desired_strength" id="desired_strength_6" value="6" onchange="updateAmounts()" checked />
				<label for="desired_strength_6">6</label>
				<input type="radio" name="desired_strength" id="desired_strength_12" value="12" onchange="updateAmounts()" />
				<label for="desired_strength_12">12</label>
				<input type="radio" name="desired_strength" id="desired_strength_18" value="18" onchange="updateAmounts()" />
				<label for="desired_strength_18">18</label>
				<input type="radio" name="desired_strength" id="desired_strength_24" value="24" onchange="updateAmounts()" />
				<label for="desired_strength_24">24</label>
				<input type="radio" name="desired_strength" id="desired_strength_36" value="36" onchange="updateAmounts()" />
				<label for="desired_strength_36">36</label>
			</div>
			<div class="input_cap">mg</div>
		</li>
	</ul>
</fieldset>

<style type="text/css">
ul.output_fields div.label{
	display:inline-block;
	max-width:180px;
	overflow:hidden;
	text-overflow:ellipsis;
	white-space:nowrap;
}
</style>

<script type="text/javascript">
function updateAmounts(){
	var	id_assigned = null,
		remaining_percent = null,
		volume_desired = document.getElementById('desired_amount').value,
		sum_ingredients = 0,
		desired_strength = 6;	//Default

	/* Get Selected Radio Value */
	desired_strength = getCheckedRadioValue_byName('desired_strength')

	for(var ingredient in JSON_recipe.ingredients){
		id_assigned = ingredient.toLowerCase()
		id_assigned = id_assigned.replace(/ /g,'_')		//Replace ALL spaces with underscore
		id_assigned = id_assigned.replace(/\(|\)/g,'')	//Replace ALL )( parenthesis with null string(0)
		document.getElementById(id_assigned).innerHTML = Math.round( volume_desired * (JSON_recipe.ingredients[ingredient]) )/100
		sum_ingredients = sum_ingredients*1 + JSON_recipe.ingredients[ingredient]*1
	}

	document.getElementById('nicotine_juice').innerHTML = Math.round( volume_desired * desired_strength )/100

	remaining_percent = 100 - sum_ingredients - desired_strength
	document.getElementById('dilutant_vg').innerHTML = Math.round( volume_desired * remaining_percent )/100
}

function getCheckedRadioValue_byName(name) {
    var elements = document.getElementsByName(name);

    for (var i=0; i<elements.length; i++)
    	if (elements[i].checked)
    		return elements[i].value;

    return '';
}

window.addEventListener('load',function(){
	updateAmounts();
},false);
</script>


<fieldset><h1>Make this:</h1>
<ul class="output_fields">
	<li>
		<div class="label">Nicotine</div>
		<div class="value" id="nicotine_juice"></div>
		<div class="input_cap">ml</div>
	</li>
	<li>
		<div class="label">Dilutant VG</div>
		<div class="value" id="dilutant_vg"></div>
		<div class="input_cap">ml</div>
	</li>
	<hr>
<?php
	$id_assigned = '';
	foreach( (array)$recipe['ingredients'] as $key => $percent):
		$id_assigned = strtolower($key);
		$id_assigned = str_ireplace(' ','_',$id_assigned);
		$id_assigned = str_ireplace('(','',$id_assigned);
		$id_assigned = str_ireplace(')','',$id_assigned);
?>

	<li>
		<div class="label"><?php echo $key ?></div>
		<div class="value" id="<?php echo $id_assigned ?>"></div>
		<div class="input_cap">ml</div>
	</li>

<?php endforeach ?>
</ul>

</fieldset>

<button onclick="doit('doit',true,true)">Exit</button>
<?php if($_COOKIE['is_admin']): ?>
<button onclick="window.location = '/edit.php<?php echo ($_GET['recipe_id']?'?recipe_id='.$_GET['recipe_id']:'') ?>'">Editor</button>
<?php endif; ?>
</body>

</html>
<?php $SQLi->close() ?>