<?php
if(!$_COOKIE['is_admin']):
	header('Location: /');
	exit;
endif;
?>
<?php
require_once 'CONFIG.php';
$SQLi = mysqli_connect( HOST , USERNAME , PASSWORD , DATABASE );
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<script type="text/javascript">
			function new_add_ingredient(to_this){
				var	li_wrapper = document.createElement('li'),
					input_ingredient = document.createElement('input'),
					input_percent = document.createElement('input');
				input_ingredient.type = 'text'
				input_ingredient.name = 'new_ingredient[]'
				input_percent.type = 'number'
				input_percent.step = '0.01'
				input_percent.name = 'percent[]'
				li_wrapper.appendChild(input_ingredient)
				li_wrapper.appendChild(input_percent)
				document.getElementById(to_this).appendChild(li_wrapper)
			}
			function isElement(element){
				var tagname = false
				try{
					tagname = element.tagName.toLowerCase()	//If .tagName is not a property... .toLowerCase() will fail on it
					return true
				}catch(e){
					return false
				}
				return false
			}
			function isInputAndHasNameAndValue(element){
				if(	element.tagName.toLowerCase() == 'input' &&
					element.name != '' &&
					element.value != ''
					)return true;
				return false;
			}
		</script>
		<script type="text/javascript">
		function submitThis(form){
			var url='processor.php'
			var variables = ''
			
			for (var element in form.elements){
				if(isElement(form.elements[element])){
					if( isInputAndHasNameAndValue(form.elements[element]) ){
						if(form.elements[element].type.toLowerCase()=='checkbox')
							variables+= form.elements[element].name+'='+(form.elements[element].checked?1:0)+'&';
						else
							variables+= form.elements[element].name+'='+form.elements[element].value+'&';
					}
				}
			}
			var last_character  = variables.substr(variables.length-1)
			if( last_character == '&') variables = variables.substr(0,variables.length-1)

			var request=new XMLHttpRequest();
				request.open('POST', url+'?nocache='+ new Date().getTime() ,true);  // Cache Control friendly
				request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				request.send(variables)
				request.onreadystatechange = function (aEvt) {
			if (request.readyState == 4) {

				if(request.status == 200)
					response_callback_function(request.responseText)
				 else
					dump("There was some kind of Error with the xml_http_request.");
				}/*end if-else successful request status */

			}/*endif request readystate changed */
			return false
		}

		function response_callback_function(response_text){
			//alert(response_text)
			var JSON_response = JSON.parse(response_text)
			if( JSON_response.success ){
				alert('All Good')
				window.location.reload()
			}else{
				alert('Something got fucked')
			}
		}
		function removeAll(){
			var fs = document.querySelectorAll('form')
			for(var f in fs) try{if(fs[f].className.indexOf('inline')==-1) fs[f].style.display='none'}catch(e){}
			return false;
		}
		</script>
		<style type="text/css">
			form fieldset ul{
				list-style:none;
				padding:none;
			}
			form fieldset ul li div:first-of-type,
			form fieldset ul li input[type=text]{
				display:inline-block;
				margin-right:1em;
				text-align:left;
				width:200px;
			}
			form fieldset ul li div:last-of-type,
			form fieldset ul li input[type=number]{
				display:inline-block;
				text-align:left;
				width:60px;
			}
			form button.right{
				float:right;
			}
			
			form#add_new,
			form#add_new_user{
				display:none;
			}
			
			form.inline,
			form.inline fieldset{
				display:inline-block;
			}
			
		</style>
		<style type="text/css">
			fieldset.pop{
				border:none;
				box-shadow:0 0 25px 0 #000;
				border-radius:15px;
				margin:2em auto;
				max-width:400px;
			}
			fieldset.pop > legend{
				background:#444;
				border-radius:15px;
				box-shadow:inset 1px 1px 7px 0 #ccc;
				color:DodgerBlue;
				font-weight:bold;
				font-size:18px;
				letter-spacing:0.15em;
				padding:0.1em 2em;
			}
			fieldset.pop > legend > button{
				margin-left:2em;
			}
			ul{
				list-style:none;
				margin:0;
				padding:0;
			}
			ul li{
				text-align:right;
				padding:0.25em 0;
			}

		</style>
	</head>
	<body>

		<form class="inline" name="select_new_or_modify" action="" method="GET">
			<fieldset>
				<button type="button" name="action" value="add_new" onclick="removeAll();document.getElementById('add_new').style.display='block';">Add New</button>
				or
				<select name="recipe_id" onchange="document.forms['select_new_or_modify'].submit()">
					<option disabled selected value=''>Edit existing Recipe</option>
					<option disabled value=''></option>
					<?php
						$query =	"SELECT * FROM
										recipes
									WHERE
										`parent_id` = 0
									ORDER BY
										`name` ASC
									;";
						$result = $SQLi->query($query);
						while( $recipe = $result->fetch_assoc() ):
					?>
						<option value="<?php echo $recipe['id'] ?>"><?php echo $recipe['name'] ?></option>
					<?php endwhile ?>
				</select>
			</fieldset>
		</form>

		<form class="inline" name="select_new_or_modify_user" action="" method="GET">
			<fieldset>
				<button type="button" name="action" value="add_new" onclick="removeAll();document.getElementById('add_new_user').style.display='block';">Add New</button>
				or
				<select name="user_id" onchange="document.forms['select_new_or_modify_user'].submit()">
					<option disabled selected value=''>Edit existing User</option>
					<option disabled value=''></option>
					<?php
						$query =	"SELECT * FROM
										users
									ORDER BY
										`user` ASC
									;";
						$result = $SQLi->query($query);
						while( $user = $result->fetch_assoc() ):
					?>
						<option value="<?php echo $user['id'] ?>"><?php echo $user['user'].($user['is_admin']?' (admin)':'') ?></option>
					<?php endwhile ?>
				</select>
			</fieldset>
		</form>

		<form id="add_new_user" action="" method="post" onsubmit="return submitThis(this);">
			<fieldset class="pop"><legend>New User</legend>
				<input type="text" name="user" placeholder="username" required/><label>Username</label><br/>
				<input type="text" name="pass" placeholder="password" required/><label>Password</label><br/>
				<input id="101" type="checkbox" name="is_admin" /><label for="101"> is Admin</label>
				<input type="hidden" name="action" value="add_new_user" />
				<button type="submit" class="right">SUBMIT</button>
			</fieldset>
		</form>

<?php if($_GET['user_id']): ?>
		<?php
		$query =	"SELECT * FROM
						users
					WHERE
						`id` = ".$_GET['user_id']."
					LIMIT
						1
					;";
		$result = $SQLi->query($query);
		$user = $result->fetch_assoc();
		?>
		<form id="modify_user" action="" method="post" onsubmit="return submitThis(this);">
			<fieldset class="pop"><legend>Modify User</legend>
				<input type="text" name="user" required value="<?php echo $user['user'] ?>"/><label>Username</label><br/>
				<input type="text" name="pass" required value="<?php echo $user['pass'] ?>"/><label>Password</label><br/>
				<input id="102" type="checkbox" name="is_admin" <?php echo ($user['is_admin']?'checked':'') ?>/><label for="102"> is Admin</label><br/>
				<input type="hidden" name="id" value="<?php echo $user['id'] ?>"/>
				<input type="hidden" name="action" value="modify_user" />
				<button type="submit" class="right">Save '<?php echo $user['user'] ?>'</button>
			</fieldset>
		</form>
<?php endif ?>

		<form id="add_new" action="" method="post" onsubmit="return submitThis(this);">
			<fieldset class="pop"><legend>New Recipe Title</legend>
				<input type="text" name="recipe_name" />
			</fieldset>
			<fieldset class="pop">
				<legend>
					New Ingredients
					<button class="" type="button" onclick="new_add_ingredient('add_new_ingredients')">Add Another</button>
				</legend>
				<ul id="add_new_ingredients">
					<li><div>Name</div><div>Percent</div></li>
					<li><input type="text" name="new_ingredient[]" /><input type="number" name="percent[]" step="0.01" /></li>
				</ul>
				<input type="hidden" name="action" value="recipe_new" />
				<p><button type="submit" class="right">SUBMIT</button></p>
			</fieldset>
		</form>

		<?php if($_GET['recipe_id']): ?>
		<form id="modify" action="" method="post" onsubmit="return submitThis(this);">
			<fieldset class="pop"><legend>Recipe Title</legend>
			<?php
			$query =	"SELECT * FROM
							recipes
						WHERE
							`id` = ".$_GET['recipe_id']."
						LIMIT
							1
						;";
			$result = $SQLi->query($query);
			$recipe = $result->fetch_assoc();
			$recipe_name = $recipe['name'];
			?>
				<input type="text" name="recipe_name" value="<?php echo $recipe_name ?>"/>
			</fieldset>
			<fieldset class="pop">
				<legend>
					Ingredients
					<button class="" type="button" onclick="new_add_ingredient('modify_ingredients')">Add Another</button>
				</legend>
				<ul id="modify_ingredients">
					<li><div>Name</div><div>Percent</div></li>
				<?php
				$query =	"SELECT * FROM
								recipes
							WHERE
								`parent_id` = ".$_GET['recipe_id']."
							ORDER BY
								`name` ASC
							;";
				$result = $SQLi->query($query);
				while($ingredient = $result->fetch_assoc()):
				?>
					<li><input type="text" name="ingredient[]" value="<?php echo $ingredient['name'] ?>" /><input type="number" name="percent[]" step="0.01" value="<?php echo $ingredient['percent'] ?>" /><input type="hidden" name="id[]" value="<?php echo $ingredient['id'] ?>" /></li>
				<?php endwhile ?>
				</ul>
				<input type="hidden" name="action" value="recipe_modify" />
				<input type="hidden" name="recipe_id" value="<?php echo $_GET['recipe_id'] ?>"/>
				<p><button type="submit" class="right">SAVE '<?php echo $recipe_name ?>'</button></p>
			</fieldset>
		</form>
		<?php endif ?>
		
		<button onclick="window.location = '/<?php echo ($_GET['recipe_id']?'?recipe_id='.$_GET['recipe_id']:'') ?>'">Viewer</button>
		
	</body>
</html>
<?php $SQLi->close() ?>